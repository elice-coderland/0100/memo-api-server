const { Router } = require('express');
const router = Router();
const { getAllMemos, getMemosByAuthor, getMemoById, createMemo, updateMemo, deleteMemo } = require('../controllers/MemosControllers');

router.get('/', getAllMemos);
router.get('/author/:author', getMemosByAuthor);
router.get('/:id', getMemoById);
router.post('/', createMemo);
router.put('/:id', updateMemo);
router.delete('/:id', deleteMemo);

module.exports = router;