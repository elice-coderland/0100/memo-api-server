const express = require('express');
const cors = require('cors');
const app = express();
const PORT = 8080;
const memosRouter = require('./routes/routers')

app.use(cors({
  origin: 'http://127.0.0.1:5500', // allow requests from this origin
  methods: ['GET', 'POST', 'PUT', 'DELETE'], // allow these HTTP methods
  allowedHeaders: ['Content-Type', 'Authorization'] // allow these headers in requests
}));
app.use(express.json());

app.get('/', (req, res) => {
    res.send('Hello, Memo!');
})

app.use('/memos', memosRouter);

app.use((err, req, res, next) => {
    const errorMessage = err.stack.split('\n')[0];
    console.error(errorMessage);
    if (err instanceof SyntaxError) {
      res.status(400).send('Invalid JSON format in request body!');
    } else if (err.name === 'ValidationError') {
      res.status(400).send(err.message);
    } else if (err.name === 'CastError') {
      res.status(400).send('Invalid ID format!');
    } else if (err.name === 'NotFoundError') {
      res.status(404).send(err.message);
    } else {
      res.status(500).send('Something broke!');
    }
  });

  

app.listen(PORT, () => {
    console.log(`The Server is running at PORT 8080`);
})