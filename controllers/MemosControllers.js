const memo = require('../models/memo');

exports.getAllMemos = (req, res, next) => {
    try {
        const memos = memo.getAllMemosFromDB();
        res.status(200).send(memos);
    } catch (err) {
        next(err);
    }
};

exports.getMemosByAuthor = (req, res, next) => {
    const { author } = req.params;
    try {
        const memos = memo.getMemosByAuthorFromDB(author);
        res.status(200).send(memos);
    } catch (err) {
        next(err);
    }
}

exports.getMemoById = (req, res, next) => {
    const { id } = req.params;
    try {
        const memoById = memo.getMemoByIdFromDB(id);
        res.status(200).send(memoById);
    } catch (err) {
        next(err);
    }
}

exports.createMemo = (req, res, next) => {
    const { author, title, content } = req.body;
    try {
        const newMemo = memo.postNewMemoInDB(author, title, content);
        res.status(200).send(newMemo);
    } catch (err) {
        next(err)
    }
}

exports.updateMemo = (req, res, next) => {
    const { id } = req.params;
    const { title, content } = req.body;
    try {
        const updatedMemo = memo.updateMemoInDB(id, title, content);
        res.status(200).send(updatedMemo);
    } catch (err) {
        next(err)
    }
}

exports.deleteMemo = (req, res, next) => {
    const { id } = req.params;
    try {
        const memoList = memo.deleteMemoInDB(id);
        res.status(200).send(memoList);
    } catch (err) {
        next(err);
    }
}