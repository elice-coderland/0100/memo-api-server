const memoPosts = [
    {id: 1, author: 'peter', title: 'memo1', content: 'hello world1'},
    {id: 2, author: 'peter', title: 'memo2', content: 'hello world2'},
    {id: 3, author: 'anthony', title: 'memo3', content: 'hello world3'},
    {id: 4, author: 'peter', title: 'memo4', content: 'hello world4'},
    {id: 5, author: 'anthony', title: 'memo5', content: 'hello world5'},
    {id: 6, author: 'steven', title: 'memo6', content: 'hello world6'},
    {id: 7, author: 'steven', title: 'memo7', content: 'hello world7'},
    {id: 8, author: 'bruce', title: 'memo8', content: 'hello world8'},
    {id: 9, author: 'barry', title: 'memo9', content: 'hello world9'},
    {id: 10, author: 'clark', title: 'memo10', content: 'hello world10'},
];

module.exports = memoPosts;