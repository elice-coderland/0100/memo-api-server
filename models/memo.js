const memos = require('../data');

exports.getAllMemosFromDB = () => {
    return memos.map(({ author, title, content }) => ({ author, title, content }))
}

exports.getMemosByAuthorFromDB = (author) => {
    const memosByAuthor = memos.filter((memo) => memo.author == author);

    if (memosByAuthor.length < 1) throw new Error('There are no memos.');
    return memosByAuthor
}

exports.getMemoByIdFromDB = (id) => {
    const memoById = memos.find((memo) => memo.id == id);
    if (!memoById) throw new Error('There is no such memo with the id.');
    
    return memoById;
}

exports.postNewMemoInDB = (author, title, content) => {
    const lastId = memos[memos.length -1].id;
    const newMemo = {
        id: lastId+1,
        author,
        title,
        content
    };
    memos.push(newMemo);
    return newMemo;
}

exports.updateMemoInDB = (id, title, content) => {
    const idx = memos.findIndex((memo) => memo.id == id);

    const willUpdate = memos[idx];
    willUpdate.title = title;
    willUpdate.content = content;
    
    return willUpdate;
}

exports.deleteMemoInDB = (id) => {
    const idx = memos.findIndex((memo) => memo.id == id);
    
    const newMemosList = memos.filter((memo) => memo.id !== id);
    return newMemosList;
}